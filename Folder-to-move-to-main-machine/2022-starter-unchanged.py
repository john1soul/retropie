from mcpi.minecraft import Minecraft
from mcpi import block
from   time import sleep
import random
import math
pi = math.pi
sin = math.sin
cos = math.cos

def init():
	ipString = "127.0.0.1"
	#ipString = "192.168.7.226"
	#mc = Minecraft.create("127.0.0.1", 4711)
	mc = Minecraft.create(ipString, 4711)
	#mc.setting("world_immutable",False)
	mc.setting("world_immutable",True)
	#x, y, z = mc.player.getPos()  
	return mc

def check(mc,x,y,z):
	air = 0
	mc.setBlocks(-127,0,-127,128,0,128,air)
	mc.setBlocks(x,y-10,z,x,y+2,z,35,15)# 0,y,0 black
	mc.setBlocks(x+5,y-10,z+5,x+5,y+127,z+5,35,14) #red
	mc.setBlocks(x+5,y-10,z-5,x+5,y+127,z-5,35,5) #green
	mc.setBlocks(x-5,y-10,z-5,x-5,y+127,z-5,35,3) #blue
	mc.setBlocks(x-5,y-10,z+5,x-5,y+127,z+5,35,4) #yellow
	
'''
wool 35,3  LIGHT BLUE 
wool 35,4  YELLOW
wool 35,5  GREEN
wool 35,14  RED

'''
def obj1(mc,x,y,z):
	mc.setBlocks(x,y-10,z,x,y+2,z,45)# 0,y,0 black
	
def mike(mc,h,k,l):
	mc.setBlock(h+1,k,l+2,35,15)
	mc.setBlock(h+2,k,l+2,35,15)
	mc.setBlock(h+3,k,l+2,35,15)
	mc.setBlock(h+4,k,l+2,35,15)
	mc.setBlock(h+8,k,l+2,35,15)
	mc.setBlock(h+9,k,l+2,35,15)
	mc.setBlock(h+10,k,l+2,35,15)
	mc.setBlock(h+11,k,l+2,35,15)
	mc.setBlock(h+11,k+1,l+2,35,15)
	mc.setBlock(h+1,k+1,l+2,35,15)
	mc.setBlock(h+2,k+1,l+2,35,13)
	mc.setBlock(h+10,k+1,l+2,35,13)
	mc.setBlock(h+3,k+1,l+2,35,5)
	mc.setBlock(h+9,k+1,l+2,35,5)
	mc.setBlock(h+8,k+1,l+2,35,15)
	mc.setBlock(h+4,k+1,l+2,35,15)
	mc.setBlock(h+2,k+2,l+2,35,15)
	mc.setBlock(h+4,k+2,l+2,35,15)
	mc.setBlock(h+8,k+2,l+2,35,15)
	mc.setBlock(h+10,k+2,l+2,35,15)
	mc.setBlock(h+3,k+2,l+2,35,5)
	mc.setBlock(h+9,k+2,l+2,35,5)
	mc.setBlock(h+-2,k+2,l+2,35,15)
	mc.setBlock(h+14,k+2,l+2,35,15)
	mc.setBlock(h+2,k+3,l+2,35,15)	
	mc.setBlock(h+2,k+4,l+2,35,15)
	mc.setBlock(h+3,k+3,l+2,35,5)
	mc.setBlock(h+3,k+4,l+2,35,13)
	mc.setBlock(h+4,k+3,l+2,35,15)
	mc.setBlock(h+4,k+4,l+2,35,15)
	mc.setBlock(h+5,k+4,l+2,35,15)
	mc.setBlock(h+6,k+4,l+2,35,15)
	mc.setBlock(h+7,k+4,l+2,35,15)
	mc.setBlock(h+8,k+4,l+2,35,15)
	mc.setBlock(h+8,k+3,l+2,35,15)
	mc.setBlock(h+9,k+3,l+2,35,5)
	mc.setBlock(h+9,k+4,l+2,35,13)
	mc.setBlock(h+10,k+3,l+2,35,15)	
	mc.setBlock(h+10,k+4,l+2,35,15)
	mc.setBlock(h+-2,k+3,l+2,35,5)
	mc.setBlock(h+-2,k+4,l+2,35,5)
	mc.setBlock(h+-2,k+5,l+2,35,5)
	mc.setBlock(h+-2,k+6,l+2,35,5)
	mc.setBlock(h+-2,k+7,l+2,35,5)
	mc.setBlock(h+-3,k+3,l+2,35,15)
	mc.setBlock(h+-3,k+4,l+2,35,15)
	mc.setBlock(h+-3,k+5,l+2,35,15)
	mc.setBlock(h+-3,k+6,l+2,35,15)
	mc.setBlock(h+-3,k+7,l+2,35,15)
	mc.setBlock(h+-1,k+3,l+2,35,15)
	mc.setBlock(h+-1,k+4,l+2,35,15)
	mc.setBlock(h+-1,k+5,l+2,35,15)
	mc.setBlock(h+-1,k+6,l+2,35,15)
	mc.setBlock(h+-1,k+7,l+2,35,15)
	mc.setBlock(h+13,k+3,l+2,35,15)
	mc.setBlock(h+13,k+4,l+2,35,15)
	mc.setBlock(h+13,k+5,l+2,35,15)
	mc.setBlock(h+13,k+6,l+2,35,15)
	mc.setBlock(h+13,k+7,l+2,35,15)
	mc.setBlock(h+14,k+4,l+2,35,5)
	mc.setBlock(h+14,k+5,l+2,35,5)
	mc.setBlock(h+14,k+6,l+2,35,5)
	mc.setBlock(h+14,k+3,l+2,35,5)
	mc.setBlock(h+14,k+7,l+2,35,5)
	mc.setBlock(h+15,k+3,l+2,35,15)
	mc.setBlock(h+15,k+4,l+2,35,15)
	mc.setBlock(h+15,k+5,l+2,35,15)
	mc.setBlock(h+15,k+6,l+2,35,15)
	mc.setBlock(h+15,k+7,l+2,35,15)
	mc.setBlock(h+1,k+5,l+2,35,15)
	mc.setBlock(h+2,k+5,l+2,35,5)
	mc.setBlock(h+3,k+5,l+2,35,5)
	mc.setBlock(h+4,k+5,l+2,35,5)
	mc.setBlock(h+5,k+5,l+2,35,5)
	mc.setBlock(h+6,k+5,l+2,35,5)
	mc.setBlock(h+7,k+5,l+2,35,5)
	mc.setBlock(h+8,k+5,l+2,35,5)
	mc.setBlock(h+9,k+5,l+2,35,5)
	mc.setBlock(h+10,k+5,l+2,35,5)
	mc.setBlock(h+11,k+5,l+2,35,15)
	mc.setBlock(h+12,k+6,l+2,35,15)
	mc.setBlock(h+1,k+6,l+2,35,5)
	mc.setBlock(h+2,k+6,l+2,35,5)
	mc.setBlock(h+3,k+6,l+2,35,0)
	mc.setBlock(h+4,k+6,l+2,35,0)
	mc.setBlock(h+1,k+6,l+2,35,5)
	mc.setBlock(h+0,k+6,l+2,35,15)
	mc.setBlock(h+5,k+6,l+2,35,0)
	mc.setBlock(h+6,k+6,l+2,35,0)
	mc.setBlock(h+7,k+6,l+2,35,0)
	mc.setBlock(h+8,k+6,l+2,35,5)
	mc.setBlock(h+9,k+6,l+2,35,5)
	mc.setBlock(h+10,k+6,l+2,35,5)
	mc.setBlock(h+11,k+6,l+2,35,13)
	mc.setBlock(h+0,k+7,l+2,35,13)
	mc.setBlock(h+1,k+7,l+2,35,0)
	mc.setBlock(h+2,k+7,l+2,35,0)
	mc.setBlock(h+3,k+7,l+2,35,5)
	mc.setBlock(h+4,k+7,l+2,35,5)
	mc.setBlock(h+5,k+7,l+2,35,5)
	mc.setBlock(h+6,k+7,l+2,35,5)
	mc.setBlock(h+7,k+7,l+2,35,5)
	mc.setBlock(h+8,k+7,l+2,35,5)
	mc.setBlock(h+9,k+7,l+2,35,5)
	mc.setBlock(h+10,k+7,l+2,35,5)
	mc.setBlock(h+11,k+7,l+2,35,5)
	mc.setBlock(h+12,k+7,l+2,35,13)
	mc.setBlock(h+-2,k+8,l+2,35,15)
	mc.setBlock(h+-2,k+9,l+2,35,15)
	mc.setBlock(h+-1,k+8,l+2,35,5)
	mc.setBlock(h+-1,k+9,l+2,35,5)
	mc.setBlock(h+-1,k+10,l+2,35,15)
	mc.setBlock(h+-1,k+11,l+2,35,15)
	mc.setBlock(h+-1,k+12,l+2,35,15)
	mc.setBlock(h+0,k+8,l+2,35,13)
	mc.setBlock(h+1,k+8,l+2,35,5)
	mc.setBlock(h+2,k+8,l+2,35,5)
	mc.setBlock(h+3,k+8,l+2,35,5)
	mc.setBlock(h+4,k+8,l+2,35,5)
	mc.setBlock(h+5,k+8,l+2,35,5)
	mc.setBlock(h+6,k+8,l+2,35,5)
	mc.setBlock(h+7,k+8,l+2,35,5)
	mc.setBlock(h+8,k+8,l+2,35,5)
	mc.setBlock(h+13,k+8,l+2,35,15)
	mc.setBlock(h+13,k+9,l+2,35,15)
	mc.setBlock(h+13,k+8,l+2,35,5)
	mc.setBlock(h+13,k+9,l+2,35,5)
	mc.setBlock(h+13,k+10,l+2,35,15)
	mc.setBlock(h+13,k+11,l+2,35,15)
	mc.setBlock(h+13,k+12,l+2,35,15)
	mc.setBlock(h+14,k+8,l+2,35,15)
	mc.setBlock(h+14,k+9,l+2,35,15)
	mc.setBlock(h+9,k+8,l+2,35,5)
	mc.setBlock(h+10,k+8,l+2,35,5)
	mc.setBlock(h+11,k+8,l+2,35,5)
	mc.setBlock(h+12,k+8,l+2,35,5)
	mc.setBlock(h+1,k+9,l+2,35,5)
	mc.setBlock(h+2,k+9,l+2,35,5)
	mc.setBlock(h+3,k+9,l+2,35,5)
	mc.setBlock(h+0,k+9,l+2,35,5)
	mc.setBlock(h+5,k+9,l+2,35,0)
	mc.setBlock(h+6,k+9,l+2,35,0)
	mc.setBlock(h+7,k+9,l+2,35,0)
	mc.setBlock(h+4,k+9,l+2,35,5)
	mc.setBlock(h+8,k+9,l+2,35,0)
	mc.setBlock(h+9,k+9,l+2,35,0)
	mc.setBlock(h+10,k+9,l+2,35,5)
	mc.setBlock(h+11,k+9,l+2,35,5)
	mc.setBlock(h+12,k+9,l+2,35,5)
	mc.setBlock(h+13,k+9,l+2,35,5)
	mc.setBlock(h+5,k+10,l+2,35,0)
	mc.setBlock(h+6,k+10,l+2,35,0)
	mc.setBlock(h+7,k+10,l+2,35,0)
	mc.setBlock(h+4,k+10,l+2,35,0)
	mc.setBlock(h+8,k+10,l+2,35,0)
	mc.setBlock(h+9,k+10,l+2,35,0)
	mc.setBlock(h+10,k+10,l+2,35,0)
	mc.setBlock(h+5,k+11,l+2,35,0)
	mc.setBlock(h+6,k+11,k+2,35,3)
	mc.setBlock(h+7,k+11,k+2,35,3)
	mc.setBlock(h+4,k+11,l+2,35,0)
	mc.setBlock(h+8,k+11,k+2,35,3)
	mc.setBlock(h+9,k+11,l+2,35,0)
	mc.setBlock(h+10,k+11,l+2,35,0)
	mc.setBlock(h+5,k+12,l+2,35,0)
	mc.setBlock(h+6,k+12,k+2,35,3)
	mc.setBlock(h+7,k+12,l+2,35,15)
	mc.setBlock(h+4,k+12,l+2,35,0)
	mc.setBlock(h+8,k+12,k+2,35,3)
	mc.setBlock(h+9,k+12,l+2,35,0)
	mc.setBlock(h+10,k+12,l+2,35,0)
	mc.setBlock(h+5,k+13,l+2,35,0)
	mc.setBlock(h+6,k+13,k+2,35,3)
	mc.setBlock(h+7,k+13,k+2,35,3)
	mc.setBlock(h+4,k+13,l+2,35,0)
	mc.setBlock(h+8,k+13,k+2,35,3)
	mc.setBlock(h+9,k+13,l+2,35,0)
	mc.setBlock(h+10,k+13,l+2,35,0)
	mc.setBlock(h+5,k+14,l+2,35,5)
	mc.setBlock(h+6,k+14,l+2,35,13)
	mc.setBlock(h+7,k+14,l+2,35,13)
	mc.setBlock(h+4,k+14,l+2,35,13)
	mc.setBlock(h+8,k+14,l+2,35,13)
	mc.setBlock(h+9,k+14,l+2,35,5)
	mc.setBlock(h+10,k+14,l+2,35,13)
	mc.setBlock(h+5,k+15,l+2,35,13)
	mc.setBlock(h+6,k+15,l+2,35,5)
	mc.setBlock(h+7,k+15,l+2,35,5)
	mc.setBlock(h+4,k+15,l+2,35,5)
	mc.setBlock(h+8,k+15,l+2,35,5)
	mc.setBlock(h+9,k+15,l+2,35,13)
	mc.setBlock(h+10,k+15,l+2,35,5)
	mc.setBlock(h+5,k+16,l+2,35,5)
	mc.setBlock(h+6,k+16,l+2,35,13)
	mc.setBlock(h+7,k+16,l+2,35,13)
	mc.setBlock(h+4,k+16,l+2,35,5)
	mc.setBlock(h+8,k+16,l+2,35,13)
	mc.setBlock(h+9,k+16,l+2,35,5)
	mc.setBlock(h+10,k+16,l+2,35,5)
	mc.setBlock(h+5,k+17,l+2,35,5)
	mc.setBlock(h+6,k+17,l+2,35,5)
	mc.setBlock(h+7,k+17,l+2,35,5)
	mc.setBlock(h+4,k+17,l+2,35,5)
	mc.setBlock(h+8,k+17,l+2,35,5)
	mc.setBlock(h+9,k+17,l+2,35,5)
	mc.setBlock(h+10,k+17,l+2,35,5)
	mc.setBlock(h+5,k+18,l+2,35,15)
	mc.setBlock(h+6,k+18,l+2,35,15)
	mc.setBlock(h+7,k+18,l+2,35,15)
	mc.setBlock(h+4,k+18,l+2,35,15)
	mc.setBlock(h+8,k+18,l+2,35,15)
	mc.setBlock(h+9,k+18,l+2,35,15)
	mc.setBlock(h+10,k+18,l+2,35,15)
	mc.setBlock(h+0,k+10,l+2,35,5)
	mc.setBlock(h+0,k+11,l+2,35,5)
	mc.setBlock(h+0,k+12,l+2,35,5)
	mc.setBlock(h+0,k+13,l+2,35,15)
	mc.setBlock(h+0,k+14,l+2,35,15)
	mc.setBlock(h+1,k+10,l+2,35,5)
	mc.setBlock(h+1,k+11,l+2,35,5)
	mc.setBlock(h+1,k+12,l+2,35,5)
	mc.setBlock(h+1,k+13,l+2,35,5)
	mc.setBlock(h+1,k+14,l+2,35,5)
	mc.setBlock(h+1,k+15,l+2,35,15)
	mc.setBlock(h+1,k+16,l+2,35,15)
	mc.setBlock(h+1,k+17,l+2,35,15)
	mc.setBlock(h+1,k+18,l+2,35,15)
	mc.setBlock(h+1,k+19,l+2,35,15)
	mc.setBlock(h+2,k+11,l+2,35,5)
	mc.setBlock(h+2,k+12,l+2,35,5)
	mc.setBlock(h+2,k+13,l+2,35,5)
	mc.setBlock(h+2,k+14,l+2,35,5)
	mc.setBlock(h+2,k+10,l+2,35,5)
	mc.setBlock(h+2,k+15,l+2,35,5)
	mc.setBlock(h+2,k+16,l+2,35,5)
	mc.setBlock(h+2,k+17,l+2,35,5)
	mc.setBlock(h+2,k+18,l+2,35,5)
	mc.setBlock(h+2,k+19,l+2,35,15)
	mc.setBlock(h+3,k+11,l+2,35,5)
	mc.setBlock(h+3,k+12,l+2,35,5)
	mc.setBlock(h+3,k+13,l+2,35,5)
	mc.setBlock(h+3,k+14,l+2,35,5)
	mc.setBlock(h+3,k+10,l+2,35,5)
	mc.setBlock(h+3,k+15,l+2,35,5)
	mc.setBlock(h+3,k+16,l+2,35,5)
	mc.setBlock(h+3,k+17,l+2,35,5)
	mc.setBlock(h+3,k+18,l+2,35,15)
	mc.setBlock(h+11,k+10,l+2,35,5)
	mc.setBlock(h+11,k+11,l+2,35,5)
	mc.setBlock(h+11,k+12,l+2,35,5)
	mc.setBlock(h+11,k+13,l+2,35,5)
	mc.setBlock(h+11,k+14,l+2,35,5)
	mc.setBlock(h+11,k+15,l+2,35,15)
	mc.setBlock(h+11,k+16,l+2,35,15)
	mc.setBlock(h+11,k+17,l+2,35,15)
	mc.setBlock(h+11,k+18,l+2,35,15)
	mc.setBlock(h+11,k+19,l+2,35,15)
	mc.setBlock(h+12,k+10,l+2,35,5)
	mc.setBlock(h+12,k+11,l+2,35,5)
	mc.setBlock(h+12,k+12,l+2,35,5)
	mc.setBlock(h+12,k+13,l+2,35,15)
	mc.setBlock(h+12,k+14,l+2,35,15)
	

def sphere(mc,x,y,z,r,inc):
	inc = 5
	for thetaX in range (0,360,inc):
		xRad = thetaX * (pi / 180) 
		for thetaY in range(0,360,inc):
			yRad= thetaY * (pi / 180) 
			print(xRad,yRad)
			h = (r * sin(yRad) * cos(xRad))+ x 
			k = (r * sin(yRad) * sin(xRad))+ y
			l = r * cos(yRad) + z
			mc.setBlock(h,k,l,57)
			#print(x,y,z,h,k,l)

    
    
def main():
	mc = init()
	mc.player.setPos(0,0,0)  
	x,y,z = mc.player.getPos()
	#createSphere(10,mc)  
	#sphere(mc,x,y,z,5,1)
	check(mc,x,y,z)
	mike(mc,x,y+30,z)
	mc.player.setPos(x+7 ,y+40,z+5)
	

if __name__ == "__main__":
	main()

"""
AIR                   0
STONE                 1
GRASS                 2
DIRT                  3
COBBLESTONE           4
WOOD_PLANKS           5
SAPLING               6
BEDROCK               7
WATER_FLOWING         8
WATER                 8
WATER_STATIONARY      9
LAVA_FLOWING         10
LAVA                 10
LAVA_STATIONARY      11
SAND                 12
GRAVEL               13
GOLD_ORE             14
IRON_ORE             15
COAL_ORE             16
WOOD                 17
LEAVES               18
GLASS                20
LAPIS_LAZULI_ORE     21
LAPIS_LAZULI_BLOCK   22
SANDSTONE            24
BED                  26
COBWEB               30
GRASS_TALL           31
WOOL                 35
FLOWER_YELLOW        37
FLOWER_CYAN          38
MUSHROOM_BROWN       39
MUSHROOM_RED         40
GOLD_BLOCK           41
IRON_BLOCK           42
STONE_SLAB_DOUBLE    43
STONE_SLAB           44
BRICK_BLOCK          45
TNT                  46
BOOKSHELF            47
MOSS_STONE           48
OBSIDIAN             49
TORCH                50
FIRE                 51
STAIRS_WOOD          53
CHEST                54
DIAMOND_ORE          56
DIAMOND_BLOCK        57
CRAFTING_TABLE       58
FARMLAND             60
FURNACE_INACTIVE     61
FURNACE_ACTIVE       62
DOOR_WOOD            64
LADDER               65
STAIRS_COBBLESTONE   67
DOOR_IRON            71
REDSTONE_ORE         73
SNOW                 78
ICE                  79
SNOW_BLOCK           80
CACTUS               81
CLAY                 82
SUGAR_CANE           83
FENCE                85
GLOWSTONE_BLOCK      89
BEDROCK_INVISIBLE    95
STONE_BRICK          98
GLASS_PANE          102
MELON               103
FENCE_GATE          107
GLOWING_OBSIDIAN    246
NETHER_REACTOR_CORE 247
"""
