from mcpi.minecraft import Minecraft
from mcpi import block
from   time import sleep
import random
import math
pi = math.pi
sin = math.sin
cos = math.cos

def init():
	ipString = "127.0.0.1"
	#ipString = "10.183.3.67"
	#mc = Minecraft.create("127.0.0.1", 4711)
	mc = Minecraft.create(ipString, 4711)
	#mc.setting("world_immutable",False)
	mc.setting("world_immutable",True)
	#x, y, z = mc.player.getPos()  
	mc.player.setPos(0,10,0)
	mc.setBlocks(-200,0,-200,200,100,200,0)# Air
	mc.setBlocks(-200,-3,-200,200,0,200,2)# Grass
	return mc

def check(mc,x,y,z):
	mc.setBlocks(x,y-10,z,x,y,z,35,15)
	mc.setBlocks(x+5,y-10,z+5,x+5,y+127,z+5,35,14) 
	mc.setBlocks(x+5,y-10,z-5,x+5,y+127,z-5,35,5) 
	mc.setBlocks(x-5,y-10,z-5,x-5,y+127,z-5,35,3)
	mc.setBlocks(x-5,y-10,z+5,x-5,y+127,z+5,35,4) 

def obj1(mc,x,y,z):
	mc.setBlocks(x,y-10,z,x,y+2,z,45)
	
def house(mc,x,y,z):

def pyramid(mc,x,y,z):
	x=x+12
	z=z
	i=-3
	size=16
	width=16
	while i < size:
		mc.setBlocks(x+i,y+i,z+i,x+width*2-i,y+i,z+width*2-i,24)
		i=i+1
def roof(mc,x,y,z):
	width = 7
	height = 4
	depth = 10
	for i in range(int(width/2) + 1):
		mc.setBlocks(x+i, y+height+i, z+3, x+i, y+height+i, z+3+depth, block.STAIRS_WOOD.id, 0)
		mc.setBlocks(x+width-i, y+height+i, z+3, x+width-i, y+height+i, z+3+depth, block.STAIRS_WOOD.id, 1)

		if (int(width/2) - i > 0):
			mc.setBlocks(x+1+i, y+height+i, z+3, x+width-i-1, y+height+i, z+3, block.BRICK_BLOCK.id, 0)
			mc.setBlocks(x+1+i, y+height+i, z+3+depth, x+width-i-1, y+height+i, z+3+depth, block.BRICK_BLOCK.id, 1)
def main():
	mc = init()
	mc.player.setPos(0,0,0)  
	x,y,z = mc.player.getPos()
	check(mc,x,y,z)
	house(mc,x,y+30,z)
	pyramid(mc,x,y,z)
	roof(mc,x,y,z)
	mc.player.setPos(x+7 ,y+40,z+5)
	
	
	

if __name__ == "__main__":
 main()

"""
AIR                   0
STONE                 1
GRASS                 2
DIRT                  3
COBBLESTONE           4
WOOD_PLANKS           5
SAPLING               6
BEDROCK               7
WATER_FLOWING         8
WATER                 8
WATER_STATIONARY      9
LAVA_FLOWING         10
LAVA                 10
LAVA_STATIONARY      11
SAND                 12
GRAVEL               13
GOLD_ORE             14
IRON_ORE             15
COAL_ORE             16
WOOD                 17
LEAVES               18
GLASS                20
LAPIS_LAZULI_ORE     21
LAPIS_LAZULI_BLOCK   22
SANDSTONE            24
BED                  26
COBWEB               30
GRASS_TALL           31
WOOL                 35
FLOWER_YELLOW        37
FLOWER_CYAN          38
MUSHROOM_BROWN       39
MUSHROOM_RED         40
GOLD_BLOCK           41
IRON_BLOCK           42
STONE_SLAB_DOUBLE    43
STONE_SLAB           44
BRICK_BLOCK          45
TNT                  46
BOOKSHELF            47
MOSS_STONE           48
OBSIDIAN             49
TORCH                50
FIRE                 51
STAIRS_WOOD          53
CHEST                54
DIAMOND_ORE          56
DIAMOND_BLOCK        57
CRAFTING_TABLE       58
FARMLAND             60
FURNACE_INACTIVE     61
FURNACE_ACTIVE       62
DOOR_WOOD            64
LADDER               65
STAIRS_COBBLESTONE   67
DOOR_IRON            71
REDSTONE_ORE         73
SNOW                 78
ICE                  79
SNOW_BLOCK           80
CACTUS               81
CLAY                 82
SUGAR_CANE           83
FENCE                85
GLOWSTONE_BLOCK      89
BEDROCK_INVISIBLE    95
STONE_BRICK          98
GLASS_PANE          102
MELON               103
FENCE_GATE          107
GLOWING_OBSIDIAN    246
NETHER_REACTOR_CORE 247
"""
