from mcpi.minecraft import Minecraft
from mcpi import block
from   time import sleep
import random
import math
pi = math.pi
sin = math.sin
cos = math.cos

def init():
	ipString = "127.0.0.1"
	#ipString = "10.183.3.67"
	#mc = Minecraft.create("127.0.0.1", 4711)
	mc = Minecraft.create(ipString, 4711)
	#mc.setting("world_immutable",False)
	mc.setting("world_immutable",True)
	#x, y, z = mc.player.getPos()  
	mc.player.setPos(0,10,0)
	mc.setBlocks(-50,-3,-50,50,64,50,0)
	mc.setBlocks(10,10,10,20,20,20,0)
	mc.setBlocks(-100,-3,100, -100,-3,100,4)
	return mc
	
def main():
	mc = init()
	mc.player.setPos(0,0,0)  
	x,y,z = mc.player.getPos()
