from mcpi.minecraft import Minecraft
from mcpi import block
from   time import sleep
import random
import math
pi = math.pi
sin = math.sin
cos = math.cos

def init():
	#ipString = "127.0.0.1"
	ipString = "10.183.3.67"
	#mc = Minecraft.create("127.0.0.1", 4711)
	mc = Minecraft.create(ipString, 4711)
	#mc.setting("world_immutable",False)
	mc.setting("world_immutable",True)
	#x, y, z = mc.player.getPos()  
	mc.player.setPos(0,10,0)
	mc.setBlocks(-50,-3,-50,50,64,50,0)
	mc.setBlocks(10,10,10,20,20,20,0)
	mc.setBlocks(-100,-3,100, -100,-3,100,4)
	return mc

def check(mc,x,y,z):
	mc.setBlocks(x,y-10,z,x,y,z,35,15)
	mc.setBlocks(x+5,y-10,z+5,x+5,y+127,z+5,35,14) 
	mc.setBlocks(x+5,y-10,z-5,x+5,y+127,z-5,35,5) 
	mc.setBlocks(x-5,y-10,z-5,x-5,y+127,z-5,35,3)
	mc.setBlocks(x-5,y-10,z+5,x-5,y+127,z+5,35,4) 

def obj1(mc,x,y,z):
	mc.setBlocks(x,y-10,z,x,y+2,z,45)
	
	

def house(mc,x,y,z):
	mc.player.setPos(0,10,0)
	mc.setBlocks(-50,-3,-50,50,64,50,0)
	mc.setBlocks(10,10,10,20,20,20,0)
	mc.setBlock(0,-3,0,4) #top left corner
	mc.setBlock(1,-3,0,4)
	mc.setBlock(2,-3,0,4)
	mc.setBlock(3,-3,0,4)
	mc.setBlock(4,-3,0,4)
	mc.setBlock(5,-3,0,4)
	mc.setBlock(6,-3,0,4)
	mc.setBlock(7,-3,0,4)
	mc.setBlock(8,-3,0,4)

	mc.setBlock(0,-3,0,4) #top left corner
	mc.setBlock(0,-3,1,4)
	mc.setBlock(0,-3,2,4)
	mc.setBlock(0,-3,3,4)
	mc.setBlock(0,-3,4,4)
	mc.setBlock(0,-3,5,4)
	mc.setBlock(0,-3,6,4)
	mc.setBlock(0,-3,7,4)
	mc.setBlock(0,-3,8,4)

	mc.setBlock(8,-3,0,4) #top right corner
	mc.setBlock(8,-3,1,4)
	mc.setBlock(8,-3,2,4)
	mc.setBlock(8,-3,3,4)
	mc.setBlock(8,-3,4,4)
	mc.setBlock(8,-3,5,4)
	mc.setBlock(8,-3,6,4)
	mc.setBlock(8,-3,7,4)
	mc.setBlock(8,-3,8,4) #right corner

	mc.setBlock(8,-3,8,4) #right corner
	mc.setBlock(7,-3,8,4)
	mc.setBlock(6,-3,8,4)
	mc.setBlock(5,-3,8,4)
	mc.setBlock(4,-2,8,64)
	mc.setBlock(4.-1,8,64)
	mc.setBlock(3,-3,8,4)
	mc.setBlock(2,-3,8,4)
	mc.setBlock(1,-3,8,4)
	mc.setBlock(0,-3,8,4) #left corner
	#cobblestone frame + door
	mc.setBlock(0,-2,8,5)
	mc.setBlock(8,-2,8,5)
	mc.setBlock(0,-2,0,5)
	mc.setBlock(8,-2,0,5)
	mc.setBlock(0,-1,8,5)
	mc.setBlock(8,-1,8,5)
	mc.setBlock(0,-1,0,5)
	mc.setBlock(8,-1,0,5)
	mc.setBlock(0,0,8,5)
	mc.setBlock(8,0,8,5)
	mc.setBlock(0,0,0,5)
	mc.setBlock(8,0,0,5)
	mc.setBlock(0,1,8,5)
	mc.setBlock(8,1,8,5)
	mc.setBlock(0,1,0,5)
	mc.setBlock(8,1,0,5)
	mc.setBlock(0,2,8,5)
	mc.setBlock(8,2,8,5)
	mc.setBlock(0,2,0,5)
	mc.setBlock(8,2,0,5)
	mc.setBlock(0,3,8,5)
	mc.setBlock(8,3,8,5)
	mc.setBlock(0,3,0,5)
	mc.setBlock(8,3,0,5)
	#wood pillars
	mc.setBlock(0,4,8,44)
	mc.setBlock(8,4,8,44)
	mc.setBlock(0,4,0,44)
	mc.setBlock(8,4,0,44)
	#slab pillar caps
	mc.setBlock(7,-2,0,5)
	mc.setBlock(6,-2,0,5)
	mc.setBlock(5,-2,0,5)
	mc.setBlock(4,-2,0,5)
	mc.setBlock(3,-2,0,5)
	mc.setBlock(2,-2,0,5)
	mc.setBlock(1,-2,0,5)
	mc.setBlock(7,-1,0,5)
	mc.setBlock(6,-1,0,5)
	mc.setBlock(5,-1,0,5)
	mc.setBlock(4,-1,0,5)
	mc.setBlock(3,-1,0,5)
	mc.setBlock(2,-1,0,5)
	mc.setBlock(1,-1,0,5)
	mc.setBlock(7,0,0,5)
	mc.setBlock(6,0,0,5)
	mc.setBlock(5,0,0,5)
	mc.setBlock(4,0,0,5)
	mc.setBlock(3,0,0,5)
	mc.setBlock(2,0,0,5)
	mc.setBlock(1,0,0,5)
	mc.setBlock(7,1,0,5)
	mc.setBlock(6,1,0,5)
	mc.setBlock(5,1,0,5)
	mc.setBlock(4,1,0,5)
	mc.setBlock(3,1,0,5)
	mc.setBlock(2,1,0,5)
	mc.setBlock(1,1,0,5)
	mc.setBlock(8,2,0,5)
	mc.setBlock(7,2,0,5)
	mc.setBlock(6,2,0,5)
	mc.setBlock(5,2,0,5)
	mc.setBlock(4,2,0,5)
	mc.setBlock(3,2,0,5)
	mc.setBlock(2,2,0,5)
	mc.setBlock(1,2,0,5)
	mc.setBlock(7,3,0,5)
	mc.setBlock(6,3,0,5)
	mc.setBlock(5,3,0,5)
	mc.setBlock(4,3,0,5)
	mc.setBlock(3,3,0,5)
	mc.setBlock(2,3,0,5)
	mc.setBlock(1,3,0,5)
	
		#WALL ONE
	mc.setBlock(1,-2,8,5)
	mc.setBlock(2,-2,8,5)
	mc.setBlock(3,-2,8,5)
	mc.setBlock(4,-2,8,5)
	mc.setBlock(4,-2,8,5)
	mc.setBlock(5,-2,8,5)
	mc.setBlock(6,-2,8,5)
	mc.setBlock(7,-2,8,5)
	mc.setBlock(1,-1,8,5)
	mc.setBlock(2,-1,8,5)
	mc.setBlock(3,-1,8,5)
	mc.setBlock(4,-1,8,5)
	mc.setBlock(4,-1,8,5)
	mc.setBlock(5,-1,8,5)
	mc.setBlock(6,-1,8,5)
	mc.setBlock(7,-1,8,5)
	mc.setBlock(1,0,8,5)
	mc.setBlock(2,0,8,5)
	mc.setBlock(3,0,8,5)
	mc.setBlock(4,0,8,5)
	mc.setBlock(4,0,8,5)
	mc.setBlock(5,0,8,5)
	mc.setBlock(6,0,8,5)
	mc.setBlock(7,0,8,5)
	mc.setBlock(1,1,8,5)
	mc.setBlock(2,1,8,5)
	mc.setBlock(3,1,8,5)
	mc.setBlock(4,1,8,5)
	mc.setBlock(4,1,8,5)
	mc.setBlock(5,1,8,5)
	mc.setBlock(6,1,8,5)
	mc.setBlock(7,1,8,5)
	
	mc.setBlock(1,2,8,5)
	mc.setBlock(2,2,8,5)
	mc.setBlock(3,2,8,5)
	mc.setBlock(4,2,8,5)
	mc.setBlock(4,2,8,5)
	mc.setBlock(5,2,8,5)
	mc.setBlock(6,2,8,5)
	mc.setBlock(7,2,8,5)
	mc.setBlock(1,3,8,5)
	mc.setBlock(2,3,8,5)
	mc.setBlock(3,3,8,5)
	mc.setBlock(4,3,8,5)
	mc.setBlock(4,3,8,5)
	mc.setBlock(5,3,8,5)
	mc.setBlock(6,3,8,5)
	mc.setBlock(7,3,8,5)
		#WALL TWO
	mc.setBlock(8,-2,7,5)
	mc.setBlock(8,-2,6,5)
	mc.setBlock(8,-2,5,5)
	mc.setBlock(8,-2,4,5)
	mc.setBlock(8,-2,3,5)
	mc.setBlock(8,-2,2,5)
	mc.setBlock(8,-2,1,5)
	mc.setBlock(8,-1,7,5)
	mc.setBlock(8,-1,6,5)
	mc.setBlock(8,-1,5,5)
	mc.setBlock(8,-1,4,5)
	mc.setBlock(8,-1,3,5)
	mc.setBlock(8,-1,2,5)
	mc.setBlock(8,-1,1,5)
	mc.setBlock(8,0,7,5)
	mc.setBlock(8,0,6,5)
	mc.setBlock(8,0,5,5)
	mc.setBlock(8,0,4,5)
	mc.setBlock(8,0,3,5)
	mc.setBlock(8,0,2,5)
	mc.setBlock(8,0,1,5)
	mc.setBlock(8,1,7,5)
	mc.setBlock(8,1,6,5)
	mc.setBlock(8,1,5,5)
	mc.setBlock(8,1,4,5)
	mc.setBlock(8,1,3,5)
	mc.setBlock(8,1,2,5)
	mc.setBlock(8,1,1,5)
	mc.setBlock(8,2,7,5)
	mc.setBlock(8,2,6,5)
	mc.setBlock(8,2,5,5)
	mc.setBlock(8,2,4,5)
	mc.setBlock(8,2,3,5)
	mc.setBlock(8,2,2,5)
	mc.setBlock(8,2,1,5)
	mc.setBlock(8,3,7,5)
	mc.setBlock(8,3,6,5)
	mc.setBlock(8,3,5,5)
	mc.setBlock(8,3,4,5)
	mc.setBlock(8,3,3,5)
	mc.setBlock(8,3,2,5)
	mc.setBlock(8,3,1,5)
		#WALL THREE
	mc.setBlock(0,-2,7,5)
	mc.setBlock(0,-2,6,5)
	mc.setBlock(0,-2,5,5)
	mc.setBlock(0,-2,4,5)
	mc.setBlock(0,-2,3,5)
	mc.setBlock(0,-2,2,5)
	mc.setBlock(0,-2,1,5)
	mc.setBlock(0,-1,7,5)
	mc.setBlock(0,-1,6,5)
	mc.setBlock(0,-1,5,5)
	mc.setBlock(0,-1,4,5)
	mc.setBlock(0,-1,3,5)
	mc.setBlock(0,-1,2,5)
	mc.setBlock(0,-1,1,5)
	mc.setBlock(0,0,7,5)
	mc.setBlock(0,0,6,5)
	mc.setBlock(0,0,5,5)
	mc.setBlock(0,0,4,5)
	mc.setBlock(0,0,3,5)
	mc.setBlock(0,0,2,5)
	mc.setBlock(0,0,1,5)
	mc.setBlock(0,1,7,5)
	mc.setBlock(0,1,6,5)
	mc.setBlock(0,1,5,5)
	mc.setBlock(0,1,4,5)
	mc.setBlock(0,1,3,5)
	mc.setBlock(0,1,2,5)
	mc.setBlock(0,1,1,5)
	mc.setBlock(0,2,7,5)
	mc.setBlock(0,2,6,5)
	mc.setBlock(0,2,5,5)
	mc.setBlock(0,2,4,5)
	mc.setBlock(0,2,3,5)
	mc.setBlock(0,2,2,5)
	mc.setBlock(0,2,1,5)
	mc.setBlock(0,3,7,5)
	mc.setBlock(0,3,6,5)
	mc.setBlock(0,3,5,5)
	mc.setBlock(0,3,4,5)
	mc.setBlock(0,3,3,5)
	mc.setBlock(0,3,2,5)
	mc.setBlock(0,3,1,5)	
	#mc.setBlock(0,-3,8,4) left corner
	#mc.setBlock(8,-3,8,4) right corner
	#mc.setBlock(0,-3,0,4) top left corner
	#mc.setBlock(8,-3,0,4) top right corner

	mc.player.setPos(0,10,0)
def pyramid(mc,x,y,z):
	x=x+12
	z=z
	i=8
	size=16
	width=16
	while i < size:
		mc.setBlocks(x+i,y+i,z+i,x+width*2-i,y+i,z+width*2-i,12)
		i=i+1

def main():
	mc = init()
	mc.player.setPos(0,0,0)  
	x,y,z = mc.player.getPos()
	check(mc,x,y,z)
	house(mc,x,y+30,z)
	pyramid(mc,x,y,z)
	mc.player.setPos(x+7 ,y+40,z+5)
	
	
	

if __name__ == "__main__":
 main()

"""
AIR                   0
STONE                 1
GRASS                 2
DIRT                  3
COBBLESTONE           4
WOOD_PLANKS           5
SAPLING               6
BEDROCK               7
WATER_FLOWING         8
WATER                 8
WATER_STATIONARY      9
LAVA_FLOWING         10
LAVA                 10
LAVA_STATIONARY      11
SAND                 12
GRAVEL               13
GOLD_ORE             14
IRON_ORE             15
COAL_ORE             16
WOOD                 17
LEAVES               18
GLASS                20
LAPIS_LAZULI_ORE     21
LAPIS_LAZULI_BLOCK   22
SANDSTONE            24
BED                  26
COBWEB               30
GRASS_TALL           31
WOOL                 35
FLOWER_YELLOW        37
FLOWER_CYAN          38
MUSHROOM_BROWN       39
MUSHROOM_RED         40
GOLD_BLOCK           41
IRON_BLOCK           42
STONE_SLAB_DOUBLE    43
STONE_SLAB           44
BRICK_BLOCK          45
TNT                  46
BOOKSHELF            47
MOSS_STONE           48
OBSIDIAN             49
TORCH                50
FIRE                 51
STAIRS_WOOD          53
CHEST                54
DIAMOND_ORE          56
DIAMOND_BLOCK        57
CRAFTING_TABLE       58
FARMLAND             60
FURNACE_INACTIVE     61
FURNACE_ACTIVE       62
DOOR_WOOD            64
LADDER               65
STAIRS_COBBLESTONE   67
DOOR_IRON            71
REDSTONE_ORE         73
SNOW                 78
ICE                  79
SNOW_BLOCK           80
CACTUS               81
CLAY                 82
SUGAR_CANE           83
FENCE                85
GLOWSTONE_BLOCK      89
BEDROCK_INVISIBLE    95
STONE_BRICK          98
GLASS_PANE          102
MELON               103
FENCE_GATE          107
GLOWING_OBSIDIAN    246
NETHER_REACTOR_CORE 247
"""
